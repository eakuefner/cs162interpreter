\documentclass[nocopyrightspace,10pt]{sigplanconf}
\usepackage{color}
\input{macros}
\usepackage{helvet}
\newcommandx{\applySyn}[2][1=e_f, 2={\vec{e}}]{\ensuremath{#1\leftsquigarrow [#2]}}
\newcommandx{\compSyn}[1][1=e]{\ensuremath{\%#1}}
\title{A Bytecode Interpreter for \textcolor{red}{notJScheme}}
\subtitle{CS162 Final Project, Winter 2012}
\authorinfo{Kevin Gibbons}{}{kgibbons@umail.ucsb.edu}
\authorinfo{Ethan J. Goldberg}{}{ethanjacobsongoldberg@umail.ucsb.edu}
\authorinfo{Ethan A. Kuefner}{}{eakuefner@cs.ucsb.edu}
\begin{document}
\maketitle
\begin{abstract}
We detail the various aspects of the bytecode interpreter project we have written for CS162, Programming Languages, Winter 2012. We have written an optimizing bytecode
compiler in Scala for a superset of all three major languages presented in CS162, called \textcolor{red}{notJScheme}. We have also provided a virtual machine implemented
in C++ to run these bytecodes, and provided several extensions to the language, including string comprehensions and an ``apply'' operator. Our code outperforms the
Scala reference implementation by a best-case factor of 7 for large programs.
\end{abstract}
\section{Introduction}
The purpose of this project, as originally proposed, was to provide an alternative
toolchain for the notJS language, which would first compile the source to a series of
bytecodes and then interpret the bytecodes to run the program.

In this release, we provide a full implementation of a toolchain that consists of an optimizing bytecode compiler and virtual machine for a language we are calling 
\textcolor{red}{notJScheme}, since it aggregates features of every language we have worked in in CS 162. Our compiler outputs an intermediate bytecode
representation, and our VM interprets this intermediate representation. Our code also provides implementations of several new language features to demonstrate the
extensibility and modularity of our interpreter design.

The organization of this paper is as follows. First, we give an overview of the language we are interpreting, followed by explanations of the compiler and interpreter
architectures. After this, we discuss the semantics and implementations of the new features we have added to the language, namely 
Finally, we discuss our work on the whole and provide directions for future work. At the end of the paper can be found an appendix detailing the bytecode specification.

Note also that the file \texttt{README}, which accompanies this project, contains details on running our software, utilizing the concrete syntax for our extensions, and
making use of the extensive test framework we have included.
\section{Compiler}
Our compiler uses much of the code that we have been provided for the various CS162 assignments as a starting place. We have modified the parser, contained in
\texttt{syntax.scala}, to support both \textcolor{red}{notJS} objects and \textcolor{red}{notScheme} lists, as well as the syntactic constructs for our two language
extensions.
Once the AST is built, we simply walk the tree printing out the opcodes corresponding to a specific
node. For example, the Add node will first call the subtree visitor on the left and right children and then print out the \texttt{ADD} opcode.

The compiler, contained in \texttt{compiler.scala}, also hooks the \textcolor{red}{notML} typechecker to provide optional typechecking of programs which are written in
the typecheckable subset of \textcolor{red}{notJScheme} corresponding to the \textcolor{red}{notML} language itself.

The simplicity of our method for compilation  makes adding extensions to the compiler extremely simple: typically only a single line in each of \texttt{syntax.scala} 
and \texttt{compiler.scala} are required.

\subsection{Optimization method}
We perform two simple optimizations on the AST before outputting bytecodes: constant folding and dead code elimination. To provide these, we use an object called 
treeState. Normally treeState simply contains the list of bytecodes corresponding to that subtree. However, if the subtree corresponding to a treeState is constant, 
the subtree also contains this information. When combining two treeStates in a way the compiler understands (e.g., when adding two expressions), 
we first check if both children have known values. If so, the resulting treeState will only contain the single bytecode necessary to print the combined value.

As an example consider the expression \texttt{(2+3)+x}. When combining the treeStates for \texttt{2} and \texttt{3}, the result simply becomes \texttt{LOAD 5}. 
When we then add \texttt{x}, the compiler is unable to determine this value, and so the result is \texttt{LOAD 5; LOAD\_VAR x; ADD} since in this case, the interpreter
must do the heavy lifting.

Dead code elimination is performed by having the visitor for If and While nodes not emit bytecode when the guard is known to evaluate to false, using the treeState 
information as above. Because of the simplicity of the constant folding, if a value is known it is guaranteed to be composed purely of literals and hence have no side 
effects which might alter the behavior of the program if not executed - hence we can safely not evaluate the expression at runtime without affecting the program.

\section{Interpreter}

Our interpreter has been architected as a stack machine, taking inspiration from well-known stack VMs like the CPython interpreter and the Java Virtual Machine.
At a high level, the virtual machine operates as follows. At startup, it reads the bytecodes contained in the file denoted by the argument to the interpreter
in a single pass, and builds a linear, abstract representation of the bytecodes. After this, it runs through the stream from start to finish, and if conditional or
looping constructs are encountered, the interpreter can jump to arbitrary locations in the program vector.

To help understand the interpreter, it will be helpful to provide a discussion of each of the major data structures involved in the program. First, the program vector,
as described above, is an indexable list of instructions accompanied by optional attributes, which can be either strings or numbers. To provide integers of arbitrary
precision, that is, elements in $\mathbb{Z}$, we make use of the standard GNU Multiple Precision Arithmetic library (GMP). The runtime stack is actually a vector with
wrapper functions, which allows us to more easily manage memory, since we do a lot of pushing and popping values off and onto the stack. We allow stack values to
contain strings, integers, or booleans, but also objects, lists, and closures. We will call the type that can contain each of these a ``stack value'' from now on.

We provide a global store which is just a vector containing stack values.

To support higher-order functions and closures, we make use of a stack of ``scopes'', which consist of an environment, which is a partial function mapping strings to 
store indices,
represented by a C++ (hash) map, and a pointer to the parent scope.

Parameterized function calls are handled in a typical way: when calling a
function, its parameters are pushed onto the stack in reverse order. Then
in the body of the function those values are popped from the stack and stored
in the relevant variables.

The major challenge for this part was handling scoping. We tried a number of
methods, each with tradeoffs in terms of time and memory. Initially the
environment was a tree structure (branches occuring when a closure was
executed). Lookup was done by checking the current leaf, then the parent,
and so on. This makes entering new blocks cheap (since no copies need to be
performed), but resolution of non-local variables more expensive. (We happen
to know that this is the tradeoff chosen in some modern languages, including
Javascript.) However, we decided it was preferable that entering blocks be
more expensive so that resolving non-local variables could be cheap. As such,
the current revision of the code flattens the environment on entering a new
block (maintaining a pointer to the previous environment, so that we can return
to it when a block exits).

Like in the canonical interpreter, declarations are separated from
initilizations, so that we can have mutually recursive functions. The model is
precisely the same as in the canonical interpreter.

Lists are represented by a struct we have rolled on our own, containing a store address, called the head, and a pointer to another instance of the struct, called the
tail. Lists can contain anything, including objects, unless the program being run was compiled with typechecking enabled.

Lastly, objects are represented as partial functions mapping strings to stack values, again implemented using C++ maps.

\section{Additional Features}

Towards the end of making our language more usable, and also to demonstrate that it is easy to extend our interpreter and compiler, we have provided two new language
features, called APPLY and COMPREHEND. APPLY takes advantage of a small modification to how functions are parsed and is implemented as a library for the language,
but was originally implemented as a language-level feature. COMPREHEND is itself implemented as a language feature and required modifications to the parser.

Note that we have only implemented the COMPREHEND extension in the bytecode interpreter. Since APPLY now only relies on a library function and a parser modification, it should work on the stock interpreter as well, however.

\subsection{APPLY}

The APPLY operation, takes a function $f$ and a list $[\vec e]$. It unboxes the list and then calls the function using the elements of the list in order as parameters. It can be represented by the big-step semantic rule which
follows.

\framed{
  \infrule[apply]
  {
    \tup[e_f] \eval \res[\clo]\textrm{ where }\clo = \env'\cdot f(\vec x)\rightarrow t
    \\
    |\vec x| = |\vec e| \andalso 
    \tup[{\vec e}][\env][\store_1] \eval \res[\vec v][\store_2] \andalso
    \set{x'} = f :: \vec x\\
    \res[\vec a][\store_3]=\alloc(\clo :: \vec v, \store_2) \andalso
    \env_1' = \env'[\set{x'\mapsto a}]
  }
  {
    \tup[\applySyn]\eval \tup[t][\env_1'][\store_3]
  }
}


This was originally represented as a language extension using the operator \texttt{<$\sim$} (hence our use of the symbol $\leftsquigarrow$), but we have since modified the parser to allow definition of anonymous functions, which on
the one hand, makes currying easy, and on the other hand, allows us to provide APPLY as a library function, which we have done (see file \texttt{tools.not}).

\subsection{COMPREHEND}

The COMPREHEND operation, as the name suggests, adds string comprehensions to \textcolor{red}{notJScheme} by taking strings to a list containing singleton strings for each character of the string. It can be represented by the
following big-step semantic rule. It operates on strings only, and the behavior is undefined if $e$ evaluates to anything other than a string.

\framed{
  \infrule[comprehend]
  {
    \tup[e] \eval \res[\str_1] \andalso
    \str_1 = ``x_1 x_2\cdots x_n''\\
    \ell = [``x_1'', ``x_2'', \dots, ``x_n'']
  }
  {
    \tup[\compSyn]\eval \tup[\ell][\env][\store_1]
  }
}


It was implemented by adding support for the \texttt{\%} operator to the parser and outputting the correct bytecodes, which are then handled by the interpreter in the correct manner.

\section{Analysis}

We tested our interpreter extensively against the existing bodies of tests for each of the subsets of our language, and also wrote some nontrivial programs testing the above language extensions, as well as a full complement of
standard library functions in \textcolor{red}{notJScheme}, including map, folds, filter, range, takewhile, dropwhile, and some function examples, including a nice quicksort implementation. One of the programs using these extensions
is an interactive Reverse Polish Notation (RPN) calculator.

Some of the testing done was performance testing, and we are happy to report that on our test machine (Intel Core i5-760@3.4GHz, 12GB RAM), was about seven times faster in the best case, on test cases where the generated AST was
very large, showing that are interpreter is in fact, effective, performance-wise. In cases where the stock interpreter outperformed the bytecode interpreter, this was almost certainly due to the fact that the JVM is able to optimize
on a level which we were not able to achieve in the time we had available to copmlete this project. But even in these cases, the stock interpreter bested ours by at most a factor of 2.

\section{Conclusion}
We are overall happy with how this project turned out. We set reasonable (perhaps even
ambitious) goals and have exceeded literally all of them.
This project has provided valuable insight into solving an interesting
engineering-oriented problem, and allowed us, especially towards the end, to learn some interesting
things about programming language design. If we had had more time or ended up doing this project a
second time, we would probably focus more on optimizing our interpreter for speed on computation-heavy programs 
(since bytecode intepreters in the real world exist in large part to provide speedups to
interpretation), and to this end explore more complex additions, including JIT
compilation or bytecode-level optimizations.

In addition to the above, in terms of future work, we have a list of language extensions we would 
have completed given the time and may like to complete in the future, including Scala-style currying, 
operator overloading, infix notation, and Lua-style coroutines.
\\\\
\appendix
\section{Bytecode Specification}
The bytcodes emitted by our compiler have the upside of being human-readable, to a certain extent. Each of our opcodes are ASCII characters. The exact correspondence between ASCII characters and opcodes, along with brief
descriptions of the action taken by the interpreter on encountering a given instruction, is given in \texttt{README}. The format itself is quite simple, with certain opcodes taking mandatory \emph{attributes}, which can either
be strings or numbers, which are in either case stored as simple ASCII. Attributes are delimited from the instruction stream by null terminators, and the interpreter knows when instructions are to be followed by an attribute.
\end{document}
