#! /usr/bin/env python

from sys import stdin

print("How large of cells do you want?")
cs = int(stdin.readline())

print("What was the output?")
o = int(stdin.readline())

a = []

while o > 0:
	a.append(o % cs)
	o = divmod(o, cs)[0]

print("Output:")

if cs is not 256:
	print( a[::-1] )
else:
	print(''.join([chr(int(i)) for i in a[::-1]]) ),