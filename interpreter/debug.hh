#ifndef DEBUG_HH
#define DEBUG_HH

#include "globals.hh"

#include <string>
#include <iostream>

void printstack();
void printstore();
void printprog();
void printSV(StackVal* sv);
void printScope(Scope* s);
std::string deref(Opcode op);

#endif

