package cs162.notJScheme.compiler

import java.io._
import scala.io._
import cs162.notJScheme.syntax._



//---------- COMPILER ----------

object Compiler { 

  def run(ast: AST) {
    

    var labelID = 0
    def getid() = { labelID += 1; labelID }


	// Basically just a list of things to print, but with additional structure for automagic constant folding.
	class treeState( val printme:List[String] = List(), val iam:String = "unknown", val intval:BigInt = 0, val boolval:Boolean = false, val strval:String = "") {
		def +(operand:treeState):treeState = {
			iam match {
				case "int" if operand.iam == "int" => {
					val res = intval + operand.intval
					new treeState(List("G\0" + res + "\0"), "int", res, false, "")
				}
				case "str" if operand.iam == "str" => {
					val res = strval + operand.strval
					new treeState(List("I\0" + res + "\0"), "str", 0, false, res)
				}
				case _ => new treeState(operand.printme ::: printme ::: List("+"))
			}
		}
		def -(operand:treeState):treeState = {
			iam match {
				case "int" if operand.iam == "int" => {
					val res = intval - operand.intval
					new treeState(List("G\0" + res + "\0"), "int", res, false, "")
				}
				case _ => new treeState(operand.printme ::: printme ::: List("-"))
			}
		}
		def *(operand:treeState):treeState = {
			iam match {
				case "int" if operand.iam == "int" => {
					val res = intval * operand.intval
					new treeState(List("G\0" + res + "\0"), "int", res, false, "")
				}
				case _ => new treeState(operand.printme ::: printme ::: List("*"))
			}
		}
		def /(operand:treeState):treeState = {
			iam match {
				case "int" if operand.iam == "int" => {
					val res = intval / operand.intval
					new treeState(List("G\0" + res + "\0"), "int", res, false, "")
				}
				case _ => new treeState(operand.printme ::: printme ::: List("/"))
			}
		}
		def <=(operand:treeState):treeState = {
			iam match {
				case "int" if operand.iam == "int" => {
					val res = intval <= operand.intval
					new treeState(List("H\0" + res + "\0"), "bool", 0, res, "")
				}
				case "str" if operand.iam == "str" => {
					val res = strval <= operand.strval
					new treeState(List("H\0" + res + "\0"), "bool", 0, res, "")
				}
				case _ => new treeState(operand.printme ::: printme ::: List("M"))
			}
		}
		def &&(operand:treeState):treeState = {
			iam match {
				case "bool" if operand.iam == "bool" => {
					val res = boolval && operand.boolval
					new treeState(List("H\0" + res + "\0"), "bool", 0, res, "")
				}
				case _ => new treeState(operand.printme ::: printme ::: List("&"))
			}
		}
		def ||(operand:treeState):treeState = {
			iam match {
				case "bool" if operand.iam == "bool" => {
					val res = boolval || operand.boolval
					new treeState(List("H\0" + res + "\0"), "bool", 0, res, "")
				}
				case _ => new treeState(operand.printme ::: printme ::: List("|"))
			}
		}
		def unary_!():treeState = {
			iam match {
				case "bool" => {
					val res = !boolval
					new treeState(List("H\0" + res + "\0"), "bool", 0, res, "")
				}
				case _ => new treeState(printme ::: List("!"))
			}
		}
		def ==(operand:treeState):treeState = {
			iam match {
				case "int" if operand.iam == "int" => {
					val res = intval == operand.intval
					new treeState(List("H\0" + res + "\0"), "bool", 0, res, "")
				}
				case "str" if operand.iam == "str" => {
					val res = strval == operand.strval
					new treeState(List("H\0" + res + "\0"), "bool", 0, res, "")
				}
				case _ => new treeState(operand.printme ::: printme ::: List("L"))
			}
		}
		
		def append(operand:treeState):treeState = {
			new treeState(printme ::: operand.printme)
		}
		def append(operand:List[String]):treeState = {
			new treeState(printme ::: operand)
		}
		def append(operand:String):treeState = {
			new treeState(printme ::: List(operand))
		}
		def prepend(operand:treeState):treeState = {
			new treeState(operand.printme ::: printme)
		}
		def prepend(operand:List[String]):treeState = {
			new treeState(operand ::: printme)
		}
		def prepend(operand:String):treeState = {
			new treeState(List(operand) ::: printme)
		}
		
		def print() = printme map scala.Console.print
	}
	
	
	
	
	
	
    def subtree(node: AST): treeState = node match {
      case Program(t) => {
		subtree(t)
      }
      case Seq(t1, t2) => {
		subtree(t1).append(subtree(t2))
      }
      case Assign(Var(x), rhs) => {
		subtree(rhs).append("B\0" + x + "\0")
      }
      case While(e, t) => {
		var o = subtree(e)
		
		if(!(o.iam == "bool" && !o.boolval)) { // ie not known to be false
		
			val l1 = getid()
			val l2 = getid()
			o = o.prepend("C\0" + l1 + "\0")
			o = o.append("D\0" + l2 + "\0")
			o = o.append(subtree(t))
			
			o.append(List("E\0" + l1+"\0", "C\0" + l2 + "\0"))
			
		} // if known to be false, we can just ignore it.
		else {  // we don't actually have to print the condition if it's known, because then it's guaranteed to be composed of constants.
			new treeState()
		}
      }
      case Out(e) => {
		subtree(e).append("F")
      }
      case e@Num(n) => {
		new treeState(List("G\0" + n + "\0"), "int", n, false, "")
      }
      case e@Bool(b) => {
		new treeState(List("H\0" + b + "\0"), "bool", 0, b, "")
      }
      case e@Str(str) => {
		new treeState(List("I\0" + str + "\0"), "str", 0, false, str)
      }
      case NotUnit() => {
		new treeState(List("J"), "unit", 0, false, "")
      }
      case Var(x) => {
		new treeState(List("K\0" + x + "\0"), "var", 0, false, "")
      }
      case Not(e) => {
		!subtree(e)
      }
      case Comprehend(e) => {
		subtree(e).append("m")
      }
      case BinOp(op, e1, e2) => {
		val t1 = subtree(e1)
		val t2 = subtree(e2)
		
		op match {
		  case Cons => {
		  	t1.append(t2).append("d\0"+"1\0")
		  }
		  case Add => t1 + t2
		  case Sub => t1 - t2
		  case Mul => t1 * t2
		  case Div => t1 / t2
		  case And => t1 && t2
		  case Or => t1 || t2
		  case Equal => t1 == t2
		  case Lte => t1 <= t2
		}
      }
      case If(e, t1, t2) => {
		var o = subtree(e)

		if(o.iam != "bool") { // ie unknown

			val l1 = getid()
			val l2 = getid()
	
			o = o.append("D\0" + l1 + "\0")
			o = o.append(subtree(t1))
			o = o.append(List("E\0" + l2 +"\0", "C\0" + l1+"\0"))
			
			o.append(subtree(t2)).append("C\0" + l2 + "C\0")
		}
		else if(o.boolval) { // ie known to always be true
			subtree(t1)
		}
		else { // ie known to always be false
			subtree(t2)
		}
      }
      case In(typ) => {
		val ts = typ match {
		  case NumT() => "N"
		  case StrT() => "O"
		  case _ => "PRINTER ON FIRE" // never happens
		}
		new treeState(List(ts))
      }
      case Block(vds, t) => {
		var c = List("Q")

		c = c ::: (vds map (x => "P\0" + x.x.x + "\0"))
		c = c ::: List("k")
		
		var d = new treeState(c)
		
		d = (vds map
				 (x => {
					subtree(x.e).append("B\0" + x.x.x + "\0")
				 })
				).foldLeft(d)(_.append(_))
		
		d.append(subtree(t)).append("R")
      }
      case VarBind(Var(x), e2) => {
      	subtree(e2).append("P\0" + x + "\0")
      }
      
      case Fun(Var(f), xs, t) => {
      	var c = List("W", "Q", "P\0" + f + "\0")
      	c = c ::: (xs map (x => {"P\0" + x.x + "\0"}))
      	c = c ::: List("k", "B\0" + f + "\0")
      	c = c ::: (xs map (x => {"B\0" + x.x + "\0"}))
      	
      	var d = new treeState(c)
      	
      	d.append(subtree(t)).append(List("R", "X"))
      }
      case Call(ef, es) => {
      	var c = (es.reverse) map subtree
      	var d = c.foldLeft(new treeState())(_.append(_))
      	
		d.append(subtree(ef)).append("Z\0" + es.length + "\0")
      }
      case NotList(es) => { 
		var c = (es map subtree).foldLeft(new treeState())(_.append(_))
		
		c.append(List("a", "d\0" + es.length + "\0"))
      }
      case Head(e) => {
		subtree(e).append("b")
      }
      case Tail(e) => {
		subtree(e).append("c")
      }
      case HAssign(e1, e2) => {
      	subtree(e1).append(subtree(e2)).append("i")
      }
      case TAssign(e1, e2) => {
      	subtree(e2).append(subtree(e1)).append("j")
      }
      case Access(e1, e2) => {
      	subtree(e2).append(subtree(e1)).append("g")
      }
      case MCall(er, ef, es) => {
      	
      	var c = ((es.reverse) map subtree).foldLeft(new treeState())(_.append(_))
      	c = c.append(subtree(er))
      	c = c.append(subtree(ef))
		
		c.append(List("h", "g", "Z\0" + (es.length+1) + "\0"))
      }
      case Method(xs, t) => {
      	// the top of the stack is the function itself. This is used in functions but not methods, so we have to pop.
      	var c = List("W", "Q", "l", "P\0"+"self\0")
      	
      	c = c ::: (xs map (x => {"P\0" + x.x + "\0"}))
      	c = c ::: List("k", "B\0"+"self\0")
      	c = c ::: (xs map (x => {"B\0" + x.x + "\0"}))
      	
      	var d = new treeState(c)
      	
      	d.append(subtree(t)).append(new treeState(List("R", "X")))
      }
      case Obj(fbs) => {
      	val c = (fbs map subtree).foldLeft(new treeState())(_.append(_))
      	c.append(List("e", "f\0" + fbs.length + "\0"))
      }
      case Update(e1, e2, e3) => {
      	subtree(e3).append(subtree(e2)).append(subtree(e1)).append(List("f\0"+"1\0", "l"))
      }
      case FldBind(Str(s), e) => {      	
      	subtree(e).append(new treeState(List("I\0" + s + "\0")))
      }
    }
    
    
    subtree(ast).print()
  }
}
