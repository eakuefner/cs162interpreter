#include "debug.hh"

using namespace std;

void printstack() {
	cerr << "H:" << stack.size() << ".	";
	for(unsigned int i=0; i<stack.size(); i++) { // TODO iterators or maps or something
		switch(stack[i]->mytype) {
			case IntT:
				cerr << "<Int: " << *stack[i]->val.i << ">, "; break;
			case BoolT:
				cerr << "<Bool: " << stack[i]->val.b << ">, "; break;
			case StrT:
				cerr << "<Str: " << *stack[i]->val.s << ">, "; break;
			case UnitT:
				cerr << "<Unit>, "; break;
			case ListT:
				cerr << "<List>, "; break;
			case ClosureT:
				cerr << "<Closure>, "; break;
			case ObjT:
				cerr << "<Object>, "; break;
			default:
				cerr << "%%, "; break;
		}
	}
	cerr << endl;
}

void printstore() {
	for(unsigned int i=0; i<store.size(); i++) { // TODO iterators or maps or something
		cerr << i << "," << store[i] << ",";
		printSV(store[i]);
		cerr << "; ";
	}
	cerr << endl;
}

void printprog() {
	for(auto it = program.begin(); it != program.end(); it++) {
		cerr << deref(it->opcode) << ",";
	}
	cerr << endl;
}

void printSV(StackVal* sv) {
	List * l;
	switch(sv->mytype) {
		case IntT:
			cerr << "{Int: " << *sv->val.i << "}"; break;
		case BoolT:
			cerr << "{Bool: " << sv->val.b << "}"; break;
		case StrT:
			cerr << "{Str: " << *sv->val.s << "}"; break;
		case UnitT:
			cerr << "{Unit}"; break;
		case ListT:
			l = sv->val.l;
			cerr << "{List: [";
			while (l->tail != nullptr) {
				printSV(store[l->head]);
				cout << ", ";
				l = l->tail;
			}
			printSV(store[l->head]);
			cerr << "]}";
			break;
		default:
			cerr << "%%"; break;
	}
}

void printScope(Scope* s) {
	if (s != nullptr) {
		for(auto t : s->env) {
			cerr << t.first << "," << t.second << "|";
		}
	}
	else
		cerr << "(Empty scope.)";
	cerr << endl;
}

string deref(Opcode op) {
	switch(op) {
		case NOP: return "NOP"; // not used	// 0
		case STORE: return "STORE";
		case LABEL: return "LABEL";
		case POP_JUMP_IF_FALSE: return "POP_JUMP_IF_FALSE";	// 3
		case JUMP: return "JUMP";
		case OUTPUT: return "OUTPUT";
		case LOAD_NUM: return "LOAD_NUM";			// 6
		case LOAD_BOOL: return "LOAD_BOOL";
		case LOAD_STR: return "LOAD_STR";
		case LOAD_UNIT: return "LOAD_UNIT";			// 9
		case LOAD_VAR: return "LOAD_VAR";
		case NOT: return "NOT";
		case PLUS: return "PLUS";				// 12
		case SUB: return "SUB";
		case MUL: return "MUL";
		case DIV: return "DIV";				// 15
		case AND: return "AND";
		case OR: return "OR";
		case EQUAL: return "EQUAL";				// 18
		case LEQ: return "LEQ";
		case INPUT_NUM: return "INPUT_NUM";
		case INPUT_STR: return "INPUT_STR";			// 21
		case DECLARE: return "DECLARE";
		case BEGIN_BLOCK: return "BEGIN_BLOCK";
		case END_BLOCK: return "END_BLOCK";			// 24
		case CONSTS: return "CONSTS";
		case CONST_NUM: return "CONST_NUM";
		case CONST_BOOL: return "CONST_BOOL";			// 27
		case CONST_STR: return "CONST_STR";
		case BEGIN_FUNC: return "BEGIN_FUNC";
		case END_FUNC: return "END_FUNC";			// 30
		case LOAD_FUNC: return "LOAD_FUNC";
		case EOFOP: return "EOFOP";
		case CALL: return "CALL";
		case LOAD_LIST: return "LOAD_LIST";
		case HEAD: return "HEAD";
		case TAIL: return "TAIL";
		case CONS: return "CONS";
		case LOAD_OBJ: return "LOAD_OBJ";
		case MOD_REC: return "MOD_REC";
		case GET_REC: return "GET_REC";
		case DUP_TWO: return "DUP_TWO";
		case HASSIGN: return "HASSIGN";
		case TASSIGN: return "TASSIGN";
		case END_DECLARES: return "END_DECLARES";
		case POP: return "POP";
		case COMPREHEND: return "COMPREHEND";
	}
	return "?";
}
