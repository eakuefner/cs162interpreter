package main

import (
	"bufio"
	"container/vector"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type IMPtype int

const (
	StrT = iota
	BoolT
	IntT
	UnitT
)

type StackVal struct {
	mytype IMPtype
	mystr  string
	mybool bool
	myint  int
}

type Opcode int
//FOR NEW OPCODES: First append your opcode to the end of this list.
const (
	STORE = iota
	LABEL
	POP_JUMP_IF_FALSE
	JUMP
	OUTPUT
	LOAD_NUM
	LOAD_BOOL
	LOAD_STR
	LOAD_UNIT
	LOAD_VAR
	NOT
	PLUS
	SUB
	MUL
	DIV
	AND
	OR
	EQUAL
	LEQ
	INPUT_NUM
	INPUT_STR
	BEGIN_BLOCK
	DEFINE
	END_BLOCK
)

//Instructions can really only either have an int or string attribute.
type Instruction struct {
	opcode Opcode
	strval string
	intval int
}

//This is the execution stack for our machine. We'll store StackVals in it.
var stack *vector.Vector

//This is the vector in which we'll store the program, for jumps. They're Instructions.
var program *vector.Vector

//This is the label map, which maps labels to the corresponding line numbers on which they are declared. This may seem silly, but label numbers may occur out of order, so we need to be able to add any numbered label at any time.
var labels = map[int]int{}

//This is the variable map, which maps variable names to their corresponding StackVals.
var vars = map[string]StackVal{}

//This is the input stream, which we'll use in soliciting input from the user.
var in *bufio.Reader

//prepareMachine sets up the globals and parses the bytecode input into a list of instructions.
//The beauty of this design is that we will simply be able to modify this function when we switch to real bytecodes!
func prepareMachine(r *bufio.Reader) {
	stack = new(vector.Vector)
	program = new(vector.Vector)
	in = bufio.NewReader(os.Stdin)

	line, err := r.ReadString('\n')
	line = strings.TrimSpace(line)

	//Here, we're simply going to preprocess the stream into the program Vector.
	for err == nil {
		attr := strings.TrimSpace(line[strings.Index(line, " ")+1:])
		var op string
		if strings.Index(line, " ") != -1 {
			op = line[:strings.Index(line, " ")]
		} else {
			op = strings.TrimSpace(line)
		}
		var instr Opcode
		//FOR NEW OPCODES: Second, match the bytecode representation of your opcode to its enum representation in this switch.
		switch op {
		case "STORE":
			instr = STORE
		case "LABEL":
			instr = LABEL
		case "POP_JUMP_IF_FALSE":
			instr = POP_JUMP_IF_FALSE
		case "JUMP":
			instr = JUMP
		case "OUTPUT":
			instr = OUTPUT
		case "LOAD_NUM":
			instr = LOAD_NUM
		case "LOAD_BOOL":
			instr = LOAD_BOOL
		case "LOAD_STR":
			instr = LOAD_STR
		case "LOAD_UNIT":
			instr = LOAD_UNIT
		case "LOAD_VAR":
			instr = LOAD_VAR
		case "NOT":
			instr = NOT
		case "+":
			instr = PLUS
		case "-":
			instr = SUB
		case "*":
			instr = MUL
		case "/":
			instr = DIV
		case "&":
			instr = AND
		case "|":
			instr = OR
		case "=":
			instr = EQUAL
		case "LEQ":
			instr = LEQ
		case "INPUT_NUM":
			instr = INPUT_NUM
		case "INPUT_STR":
			instr = INPUT_STR
		case "BEGIN_BLOCK":
			instr = BEGIN_BLOCK
		case "DEFINE":
			instr = DEFINE
		case "END_BLOCK":
			instr = END_BLOCK
		}

		//	fmt.Printf("Pushing opcode %d\n",instr)
		//FOR NEW OPCODES: Third, add your opcode to one of these cases if it accepts a string, a number, or a bool as an attribute.
		switch instr {
		default:
			program.Push(Instruction{opcode: instr})
		case STORE, OUTPUT, LOAD_STR, LOAD_VAR, DEFINE:
			program.Push(Instruction{opcode: instr, strval: attr})
		case LABEL:
			labelnum, err := strconv.Atoi(attr)
			if err == nil {
				labels[labelnum] = len(*program) - 1
				program.Push(Instruction{opcode: instr, intval: labelnum})
			} else {
				panic("Bad integer attribute for " + op)
			}
		case POP_JUMP_IF_FALSE, JUMP, LOAD_NUM:
			intattr, err := strconv.Atoi(attr)
			if err == nil {
				program.Push(Instruction{opcode: instr, intval: intattr})
			} else {
				panic("Bad integer attribute for " + op)
			}
		case LOAD_BOOL:
			var tempBool int
			if attr == "true" {
				tempBool = 1
			} else {
				tempBool = 0
			}
			program.Push(Instruction{opcode: instr, intval: tempBool})
		}

		line, err = r.ReadString('\n')
	}
}

func getInstr(v interface{}) Instruction {
	ret, ok := v.(Instruction)
	if !ok {
		panic("Bad instruction in vector")
	}
	return ret
}

func getSv(v interface{}) StackVal {
	ret, ok := v.(StackVal)
	if !ok {
		panic("Bad stackval in vector")
	}
	return ret
}

//runProgram acts on the vector program, and iterates until the PC is equal to the size of the vector.
func runProgram() {
	pc := 0
	size := len(*program)
	var curOp Instruction
	for pc+1 < size {
		//		fmt.Printf("PC is currently %d\n", pc)
		curOp = getInstr(program.At(pc))

//		fmt.Printf("Reading instruction %d\n", curOp.opcode)

		//FOR NEW OPCODES: Fourth, add your opcode's implementation to the below switch block. Fix the program counter afterwards.
		switch curOp.opcode {
		default:
			pc++
		case DEFINE:
			vars[curOp.strval] = getSv(stack.Pop())
			pc++
		case STORE:
			vars[curOp.strval] = getSv(stack.Pop())
			pc++
		case POP_JUMP_IF_FALSE:
			if !getSv(stack.Pop()).mybool {
				pc = labels[curOp.intval] + 1
			} else {
				pc++
			}
		case JUMP:
			pc = labels[curOp.intval] + 1
		case OUTPUT:
			temp := getSv(stack.Pop())
			switch temp.mytype {
			case StrT:
				fmt.Println(temp.mystr)
			case IntT:
				fmt.Println(temp.myint)
			case BoolT:
				fmt.Println(temp.mybool)
			case UnitT:
				fmt.Println("Unit")
			}
			pc++
		case LOAD_NUM:
			stack.Push(StackVal{mytype: IntT, myint: curOp.intval})
			pc++
		case LOAD_BOOL:
			var tempBool bool
			if curOp.intval == 1 {
				tempBool = true
			} else {
				tempBool = false
			}
			stack.Push(StackVal{mytype: BoolT, mybool: tempBool})
			pc++
		case LOAD_STR:
			stack.Push(StackVal{mytype: StrT, mystr: curOp.strval})
			pc++
		case LOAD_UNIT:
			stack.Push(StackVal{mytype: UnitT})
			pc++
		case LOAD_VAR:
			stack.Push(vars[curOp.strval])
			pc++
		case NOT:
			temp := !getSv(stack.Pop()).mybool
			stack.Push(StackVal{mytype: BoolT, mybool: temp})
			pc++
		case PLUS:
			temp1 := getSv(stack.Pop())
			temp2 := getSv(stack.Pop())
			if temp1.mytype == IntT {
				stack.Push(StackVal{mytype: IntT, myint: temp2.myint + temp1.myint})
			} else {
				stack.Push(StackVal{mytype: StrT, mystr: temp2.mystr + temp1.mystr})
			}
			pc++
		case SUB:
			temp1 := getSv(stack.Pop())
			temp2 := getSv(stack.Pop())
			stack.Push(StackVal{mytype: IntT, myint: temp2.myint - temp1.myint})
			pc++
		case MUL:
			temp1 := getSv(stack.Pop())
			temp2 := getSv(stack.Pop())
			stack.Push(StackVal{mytype: IntT, myint: temp2.myint * temp1.myint})
			pc++
		case DIV:
			temp1 := getSv(stack.Pop())
			temp2 := getSv(stack.Pop())
			stack.Push(StackVal{mytype: IntT, myint: temp2.myint / temp1.myint})
			pc++
		case AND:
			temp1 := getSv(stack.Pop())
			temp2 := getSv(stack.Pop())
			stack.Push(StackVal{mytype: BoolT, mybool: temp2.mybool && temp1.mybool})
			pc++
		case OR:
			temp1 := getSv(stack.Pop())
			temp2 := getSv(stack.Pop())
			stack.Push(StackVal{mytype: BoolT, mybool: temp2.mybool || temp1.mybool})
			pc++
		case EQUAL:
			temp1 := getSv(stack.Pop())
			temp2 := getSv(stack.Pop())
			var pushval bool
			switch temp1.mytype {
			case StrT:
				if temp2.mytype == StrT {
					pushval = (temp1.mystr == temp2.mystr)
				} else {
					pushval = false
				}
			case IntT:
				if temp2.mytype == IntT {
					pushval = (temp1.myint == temp2.myint)
				} else {
					pushval = false
				}
			case BoolT:
				if temp2.mytype == BoolT {
					pushval = (temp1.mybool == temp2.mybool)
				} else {
					pushval = false
				}
			case UnitT:
				if temp2.mytype == UnitT {
					pushval = true
				} else {
					pushval = false
				}
			}
			stack.Push(StackVal{mytype: BoolT, mybool: pushval})
			pc++
		case LEQ:
			temp1 := getSv(stack.Pop())
			temp2 := getSv(stack.Pop())
			if temp1.mytype == IntT {
				stack.Push(StackVal{mytype: BoolT, mybool: temp2.myint <= temp1.myint})
			} else {
				stack.Push(StackVal{mytype: StrT, mybool: temp2.mystr <= temp1.mystr})
			}
			pc++
		case INPUT_STR:
			tempStr, err := in.ReadString('\n')
			if err == nil {
				stack.Push(StackVal{mytype: StrT, mystr: strings.TrimSpace(tempStr)})
			} else {
				panic(err)
			}
			pc++
		case INPUT_NUM:
			tempStr, err := in.ReadString('\n')
			if err != nil {
				panic(err)
			}
			tempint, err2 := strconv.Atoi(strings.TrimSpace(tempStr))
			if err2 == nil {
				stack.Push(StackVal{mytype: IntT, myint: tempint})
				pc++
			} else {
				panic("Bad number input")
			}
		}
	}
}

func main() {
	flag.Parse()
	if flag.NArg() == 0 {
		fmt.Println("IMP Bytecode Interpreter")
		fmt.Println("usage: interpreter filename")
	} else {
		f, err := os.Open(flag.Arg(0))
		if f == nil {
			fmt.Fprintf(os.Stderr, "interpreter: can't open %s: error %s\n", flag.Arg(0), err)
			os.Exit(1)
		} else {
			prepareMachine(bufio.NewReader(f))
			runProgram()
		}
	}
}
