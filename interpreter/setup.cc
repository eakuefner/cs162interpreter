#include "setup.hh"
#include "debug.hh"
#include <list>
#include <algorithm>


// TODO maybe a parseOpLine helper func? For use with function defs.
// TODO? push_scope, pop_scope opcodes
// TODO functions = call Interpret with iterators. New stackdata type: closures (=env + 2 iterators)

/*
 *	********** Machine init. Only runs at start. **********
 */

using namespace std;

typedef pair<Opcode, string> opArgPair;

unsigned int height = 0;



list<pair<list<opArgPair>::iterator, list<opArgPair>::iterator> > funcdefs;
// TODO Actually makes more sense as a queue... but oh well.

class FuncFinder {
	public:
		FuncFinder() {depth=0;}
		bool operator () ( const opArgPair oap ) {
			if(oap.first == BEGIN_FUNC)
				depth++;
			else if(oap.first == END_FUNC)
				depth--;
			return depth == 0;
		}
		void reset() {depth=0;}
	
	private:
		int depth;
};


int fid = 0;
int getFid(){return fid++;}

void parseOps(list<opArgPair>* opsp, list<opArgPair>::iterator begin, list<opArgPair>::iterator end, bool isFunc) {
	
	opArgPair oap;
	string argstr;
	Instruction inst;
	mpz_class intarg;
	Opcode op;

	auto finder = FuncFinder();
	list<opArgPair>::iterator endFunc;

	auto ops = *opsp;


	unsigned int spc, epc;
	
	spc = program.size();

	for (auto it = begin; it != end; it++) {
		oap = *it;
		op = oap.first;
		argstr = oap.second;
	
		inst = Instruction();
		inst.opcode = op;

		StackVal* sv;

		// Handle arguments and labels
		//cerr << "parsing " << deref(op) << endl;
		switch (op) {
			case LABEL:
				//cout << ti << endl;
				//cout << argstr << endl;
				if(!(stringstream(argstr) >> intarg)) panic("Label not parsed.");
				labels[static_cast<int>(intarg.get_si())] = program.size()-1;
				inst.i = intarg;
				break;
			case STORE:
			case LOAD_VAR:
			case DECLARE:
				inst.s = argstr;
				break;
			case POP_JUMP_IF_FALSE:
			case JUMP:
				if(!(stringstream(argstr) >> intarg)) panic("Label not parsed.");
				inst.i = intarg;
				break;
			case LOAD_NUM:
			case CONS:
			case MOD_REC:
			case CALL:
				if(!(stringstream(argstr) >> intarg)) panic("Integer not parsed, with Op="+deref(CALL)+" and argstr="+argstr);
				inst.i = intarg;
				break;
			case LOAD_BOOL:
				if(argstr == "true")
					inst.b = true;
				else if(argstr == "false")
					inst.b = false;
				else
					panic("Bool not parsed.");
				break;
			case LOAD_STR:
				inst.s = argstr;
				break;
				//if(!(stringstream(argstr) >> intarg)) panic("Constant label not parsed. (1)");
				//inst->i = intarg;				
				//break; // TODO this
			case CONST_NUM:
				if(!(stringstream(argstr) >> intarg)) panic("Constant int not parsed. (2)");
				sv = new StackVal(intarg);
				consts.push_back(sv);
				break;
			case CONST_BOOL:
				if(argstr == "true")
						sv = new StackVal(true);
				else if(argstr == "false")
						sv = new StackVal(false);
				else
						panic("Bool not parsed.");
				consts.push_back(sv);
				break;
			case CONST_STR:
				sv = new StackVal(argstr);
				consts.push_back(sv);
				break; // TODO this
			case BEGIN_BLOCK:
				//finder.reset();
				//endBlock = find_if(it, ops.end(), finder);
				// TODO ?
				/*
				TODO constants are generated at the end of blocks,
				but the interpreter COMPLETELY ignores that.
				But I guess it doesn't matter?
				Constants should maybe be at the end of the program.
				*/
				//cout << *it << "," << *endBlock << endl;
				break;
			case BEGIN_FUNC:
				finder.reset();
				endFunc = find_if(it, end, finder);
				inst.opcode = LOAD_FUNC;
				inst.i = getFid();
				funcdefs.push_back(make_pair(it, endFunc));
				it = endFunc;
				break;
			default: {}
				// no arguments, nothing to do here.
		}
		
		// TODO don't push "instructions to interpreter"
		program.push_back(inst); // note: when removing labels, have to off-by-one
	}

	epc = program.size();
	if(isFunc)
		functions.push_back(make_pair(spc, epc));
	else {
		inst = Instruction();
		inst.opcode = EOFOP;
		program.push_back(inst); 
	}

	if(funcdefs.empty()) // We're done!
		return;
	
	auto nextFunc = funcdefs.front();
	funcdefs.pop_front();
	auto s = nextFunc.first;
	auto e = nextFunc.second; // TODO vector instead of lists so we can random-access
	
	parseOps(&ops, ++s, e, true);
}



void prepMachine(ifstream* f) {
	char opc;
	string argstr;
	Opcode op;
	
	
	list<opArgPair> ops;

	// For now, one opcode per line
	while(f->good() ) {
		opc = f->get();
		if (f->peek() == '\0') {
		//We're looking at a null terminator, so we'll advance past it and then read in the attribute.
			f->get();
			getline(*f, argstr, '\0');

		} else {
			argstr = "";
		}

		op = NOP; // just so g++ doesn't complain
		
		if(opc=='B')
			op = STORE;
		else if(opc=='C')
			op = LABEL;
		else if(opc=='D')
			op = POP_JUMP_IF_FALSE;
		else if(opc=='E')
			op = JUMP;
		else if(opc=='F')
			op = OUTPUT;
		else if(opc=='G')
			op = LOAD_NUM;
		else if(opc=='H')
			op = LOAD_BOOL;
		else if(opc=='I')
			op = LOAD_STR;
		else if(opc=='J')
			op = LOAD_UNIT;
		else if(opc=='K')
			op = LOAD_VAR;
		else if(opc=='!')
			op = NOT;
		else if(opc=='+')
			op = PLUS;
		else if(opc=='-')
			op = SUB;
		else if(opc=='*')
			op = MUL;
		else if(opc=='/')
			op = DIV;
		else if(opc=='&')
			op = AND;
		else if(opc=='|')
			op = OR;
		else if(opc=='L')
			op = EQUAL;
		else if(opc=='M')
			op = LEQ;
		else if(opc=='N')
			op = INPUT_NUM;
		else if(opc=='O')
			op = INPUT_STR;
		else if(opc=='Q')
			op = BEGIN_BLOCK;
		else if(opc=='P')
			op = DECLARE;
		else if(opc=='R')
			op = END_BLOCK;
		else if(opc=='S')
			op = CONSTS;
		else if(opc=='T')
			op = CONST_NUM;
		else if(opc=='U')
			op = CONST_BOOL;
		else if(opc=='V')
			op = CONST_STR;
		else if(opc=='W')
			op = BEGIN_FUNC;
		else if(opc=='X')
			op = END_FUNC;
		else if(opc=='Z')
			op = CALL;
		else if(opc=='a')
			op = LOAD_LIST;
		else if(opc=='b')
			op = HEAD;
		else if(opc=='c')
			op = TAIL;
		else if(opc=='d')
			op = CONS;
		else if(opc=='e')
			op = LOAD_OBJ;
		else if(opc=='f')
			op = MOD_REC;
		else if(opc=='g')
			op = GET_REC;
		else if(opc=='h')
			op = DUP_TWO;
		else if(opc=='i')
			op = HASSIGN;
		else if(opc=='j')
			op = TASSIGN;
		else if(opc=='k')
			op = END_DECLARES;
		else if(opc=='l')
			op = POP;
		else if(opc=='m')
			op = COMPREHEND;
		else {
			op = NOP;
			//panic("Unreadable line."); 
		}
		ops.push_back(make_pair(op, argstr));
	}
	
	
	parseOps(&ops, ops.begin(), ops.end(), false);

	//printprog();
}

void setup(ifstream* f) {
	stack.reserve(STACKSTART);
	program.reserve(PROGSTART);
	prepMachine(f);
}

