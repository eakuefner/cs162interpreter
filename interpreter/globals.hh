#ifndef GLOBALS_HH
#define GLOBALS_HH

#include <map>
#include <vector>
#include <list>
#include <string>
#include <gmpxx.h>
#include <iostream> //deal with it, while I debug.

#include "opcodes.hh"

#define STACKSTART 10
#define PROGSTART 100

typedef std::map<std::string, unsigned int> Env;
//typedef std::pair<Env, Scope*> Scope;

struct Scope {
	Env env;
	Scope* parent;
	Scope(Env ep, Scope* parentp) { env = ep; parent = parentp; }
	~Scope() {};
};

enum IMPtype {
	// UndefT, // used to mark things in store as free //JKLOL
    	StrT,
	BoolT,
	IntT,
	ClosureT,
	UnitT,
	ListT,
	ObjT,
	TYPES
};

//Instructions can really only either have an int or string attribute.
struct Instruction {
	Opcode opcode;
	std::string s;
	mpz_class i;
	bool b;
	Instruction() { opcode = NOP; s = ""; i = 0; b = false; };
};

struct Closure {
	std::vector<Instruction>::iterator fStart;
	std::vector<Instruction>::iterator fEnd;
	Scope* scope;
};

typedef std::map<std::string, int> Object;

struct List {
	int head;
	List * tail;
	List(int h, List * t) {
		head = h;
		tail = t;
	};
};

struct StackVal {
	IMPtype mytype;
	union val_t {
		mpz_class * i;
		bool b;
		std::string * s;
		Closure * c;
		List * l;
		Object * o;
	} val;

	StackVal() { mytype = UnitT; val = {0}; };
	StackVal(mpz_class x) { mytype = IntT; val.i = new mpz_class(x); };
	StackVal(bool bl) { mytype = BoolT; val.b = bl;};
	StackVal(std::string st) { mytype = StrT; val.s = new std::string(st); };
	StackVal(Closure * cl) { mytype = ClosureT; val.c = cl; };
	StackVal(List * li) { mytype = ListT; val.l = li; };
	StackVal(Object * ob) { mytype = ObjT; val.o = ob; };

	void set(StackVal sv) { mytype = sv.mytype; val = sv.val; };
	~StackVal();
};

/*
// TODO only set relevant fields
struct StackVal {
	IMPtype mytype;
	std::string s;
	mpz_class i;
	bool b;
	Closure* c;
	StackVal() { mytype = UnitT; s = ""; i = 0; b = false; c = nullptr;};
	StackVal(mpz_class x) { mytype = IntT; s = ""; i = x; b = false; c = nullptr;};
	StackVal(bool bl) { mytype = BoolT; s = ""; i = 0; b = bl; c = nullptr;};
	StackVal(std::string st) { mytype = StrT; s = st; i = 0; b = false; c = nullptr;};
	StackVal(StackVal* sv) { mytype = sv->mytype; s = sv->s; i = sv->i; b = sv->b; c = sv->c;};
	void set(StackVal* sv) { mytype = sv->mytype; s = sv->s; i = sv->i; b = sv->b; c = sv->c;};
};
*/


//using namespace std;


//TODO use the right data structures

extern std::vector<StackVal*> stack;

extern std::vector<Instruction> program;

extern std::map<int, int> labels; // label number => program counter

extern std::vector<Scope*> scopes;

extern std::vector<StackVal*> store;

extern std::vector<StackVal*> consts;

extern std::vector<unsigned int> free_store;

extern std::vector<std::pair<unsigned int, unsigned int> > functions;

void runBytes(std::vector<Instruction>::iterator, std::vector<Instruction>::iterator, Scope*);
void operate(std::string, int n);


#endif
