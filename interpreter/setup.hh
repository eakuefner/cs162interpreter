#ifndef SETUP_HH
#define SETUP_HH

#include <fstream>
#include <sstream>

#include "globals.hh"
#include "panic.hh"

using namespace std;

void prepMachine(ifstream* f);
void setup(ifstream* f);

#endif
