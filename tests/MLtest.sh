#! /bin/bash

for i in notML/*.not; do
	s=$(echo $i | cut -d'.' -f 1)
	echo
	echo
	echo
	echo "~~~~~~~~~~~~~~~~"
	echo $s
	#./compare.sh $i
	scala -classpath ../compiler notJScheme $i --typecheck
	echo ================
	cat $s.out
done
