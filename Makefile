DIRS = compiler interpreter

all: force_look
	for d in $(DIRS); do (cd $$d; make); done

clean:
	for d in $(DIRS); do (cd $$d; make clean); done

force_look:
	true
