#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>

#include "opcodes.hh"
#include "setup.hh"
#include "globals.hh"
#include "panic.hh"
#include "machinemanip.hh"
#include "debug.hh"

using namespace std;

List * cons(int x, List * l) {
	return new List(x, l);
}

bool is_in(Object * x, string s) {
	return (x->find(s) != x->end());
}

StackVal get(Object * x, string s) {
	if (is_in(x, s)) {
		return *store[(*x)[s]];
	}
	if (!is_in(x, "proto")) {
		return StackVal();
	}
	return get(store[ (*x)["proto"] ]->val.o, s);
}

void pprint(StackVal sv) {
	List * l;
	Object o;
	switch (sv.mytype) {
		case IntT:
			cout << *sv.val.i; break;
		case StrT:
			cout << *sv.val.s; break;
		case BoolT:
			if (sv.val.b) {
				cout << "true";
			} else {
				cout << "false";
			}
			break;
		case UnitT:
			cout << "unit"; break;
		case ClosureT:
			cout << "<closure>"; break;
		case ListT:
			l = sv.val.l;
			if (l == nullptr) {
				cout << "[]"; break;
			}
			cout << "[";
			while (l->tail != nullptr) {
				pprint(*store[l->head]);
				cout << ", ";
				l = l->tail;
			}
			pprint(*store[l->head]);
			cout << "]"; break;
		case ObjT:
			o = *sv.val.o;
			if (get(&o, "__print__").mytype == ClosureT) {
				push(&o);
				operate("__print__", 1);
				break;
			}
			cout << "{";
			for (auto &ele : o) {
				cout << ", " << ele.first << " : ";
				pprint(*store[ele.second]);
			}
			cout << "}"; break;
		default:
			cout << "%%"; break;
	}
}

void call(int n) {
	StackVal v;
	Closure * c;

	/* Note that we're not actually doing a pop here.
	This is so that when defining a function as eg
	var b = a() => {}
	the body of this function can refer to the function itself
	as either a or b. (This is required by the semantics. Why,
	I'm not sure.) */
	if(stack.size() == 0) panic("Calling empty stack.");
	c = closureVal(*stack.back());

	/* theight is the height of the stack before we decided
	to call the function. This is given by the current height
	minus the number of parameters, the parameters having been
	pushed onto the stack. The "- 1" is for the function itself,
	which remains on the stack.*/
	unsigned int theight = stack.size() - 1 - n;
	
	runBytes(c->fStart, c->fEnd, c->scope);
	
	if(stack.size() <= theight) { // ie, returning nothing
		push(); // push Unit
	}
	else { // Otherwise set to top of stack
		v = pop();
		while (stack.size() > theight)
			pop();
		push(v);
	}
}

void operate(string s, int n) {
	Object * o = popObj();
	push(o);
	StackVal v = get(o, s);
	type_check(ClosureT, v.mytype);
	Closure * c = closureVal(v);
	push(c);
	call(n);
}

/*
 *	********** The actual machine. **********
 */


void runBytes(vector<Instruction>::iterator startIt, vector<Instruction>::iterator endIt, Scope* parent) {
	//printprog();

	//Instruction cur;
	//StackVal* t;
	
	mpz_class i1, i2;
	string s1, s2;
	bool b1, b2;
	StackVal v1;
	StackVal v2;
	
	List * l;
	
	int index;
	
	IMPtype tos;
	
	unsigned int cnum;
	pair<unsigned int, unsigned int> func_offsets;
	Closure * c;
	Object * o;
	Env e;
	
	// TODO memory management!
	
	Scope* curscope = parent;
	Env tenv; // TODO not this


	//printScope(parent);

	for(auto pc = startIt; pc != endIt; pc++) {
		
		//cerr << "Height: " << stack.size() << "," << height << endl;
		//printstore();
		//printstack();
		//cerr << "Reading instruction " << deref(pc->opcode) << endl;
		
		
		switch (pc->opcode) {
			case STORE:
				setstore(pc->s, curscope, pop());
				break;
			case LABEL: break;
			case POP_JUMP_IF_FALSE:
				if(!popBool())
					pc = program.begin() + labels[static_cast<int>(pc->i.get_si())];
				break;
			case JUMP:
				pc = program.begin() + labels[static_cast<int>(pc->i.get_si())]; break;
			case OUTPUT:
				pprint(pop());
				cout << endl;
				break;
			case LOAD_NUM:
				push(pc->i); break;
			case LOAD_BOOL:
				push(pc->b); break;
			case LOAD_STR:
				push(pc->s); break;
			case LOAD_UNIT:
				push(); break;
			case LOAD_FUNC: // only called when defining functions
				cnum = static_cast<unsigned int>(pc->i.get_ui());
				if(cnum >= functions.size()) panic("Function reference not extant.");
				
				func_offsets = functions[cnum]; 
				c = new Closure(); // TODO leak
				c->fStart = program.begin()+func_offsets.first;
				c->fEnd = program.begin()+func_offsets.second;

				c->scope = new Scope(curscope->env, nullptr);
				
				push(c);
				break;
			case LOAD_VAR:
				v1 = *lookup(pc->s, curscope); // lookup cannot return nullptr
				push(v1);
				break;
			case NOT:
				if (typetop() == ObjT) {
					operate("__not__", 1);
				} else {
					push(!popBool());
				}
				break;
			case PLUS:
				tos = typetop();
				switch(tos) {
					case ObjT:
						operate("__add__", 2);
						break;
					case IntT:
						i1 = popInt();
						i2 = popInt();
						push(i1+i2); break;
					case StrT:
						s1 = popStr();
						s2 = popStr();
						push(s1+s2); break;
					default:
						stringstream ss;
						ss << "Trying to add " << tos << "." << endl;
						panic(ss.str());
				}
				break;
			case SUB:
				switch(typetop()) {
					case ObjT:
						operate("__sub__", 2);
						break;
					case IntT:
						i1 = popInt();
						i2 = popInt();
						push(i1-i2);
						break;
					default:
						panic("Can't subtract that.");
				} break;
			case MUL:
				switch(typetop()) {
					case ObjT:
						operate("__mul__", 2);
						break;
					case IntT:
						i1 = popInt();
						i2 = popInt();
						push(i1*i2);
						break;
					default:
						panic("Can't multiply that.");
				} break;
			case DIV:
				switch(typetop()) {
					case ObjT:
						operate("__div__", 2);
						break;
					case IntT:
						i1 = popInt();
						i2 = popInt();
						push(i1/i2);
						break;
					default:
						panic("Can't divide that.");
				} break;
			case AND:
				switch(typetop()) {
					case ObjT:
						operate("__and__", 2);
						break;
					case BoolT:
						b1 = popBool();
						b2 = popBool();
						push(b1 && b2); break;
					default:
						panic("Can't and that.");
				} break;
			case OR:
				switch(typetop()) {
					case ObjT:
						operate("__or__", 2);
						break;
					case BoolT:
						b1 = popBool();
						b2 = popBool();
						push(b1 || b2); break;
						break;
					default:
						panic("Can't or that.");
				} break;
			case EQUAL:
				v1 = pop();
				v2 = pop();
				if(v1.mytype != v2.mytype) push(false);
				else {
					switch(v1.mytype) {
						case IntT:
							push(intVal(v1) == intVal(v2)); break;
						case StrT:
							push(strVal(v1) == strVal(v2)); break;
						case BoolT:
							push(boolVal(v1) == boolVal(v2)); break;
						case ClosureT:
							push(v1.val.c == v2.val.c); break;
						case ListT:
							push(listVal(v1) == listVal(v2)); break;
						case ObjT:
							o = v1.val.o;
							if (get(o, "__equal__").mytype == ClosureT) {
								push(v2);
								push(o);
								operate("__equal__", 2);
								break;
							}
							push(objVal(v1) == objVal(v2)); break;
						case UnitT:
							push(true); break;
						default:
							panic("Type not handled. (wut)");
					}
				}
				break;
			case LEQ:
				tos = typetop();
				switch(tos) {
					case ObjT:
						operate("__leq__", 2);
						break;
					case IntT:
						i1 = popInt();
						i2 = popInt();
						push(i1 <= i2); break;
					case StrT:
						s1 = popStr();
						s2 = popStr();
						push(s1 <= s2); break;
					default:
						panic("LEQing things which can't be LEQ'd.");
				}
				break;
			case INPUT_NUM:
				if(!(cin >> skipws >> i1)) panic("Couldn't read a number.");
				push(i1);
				break;
			case INPUT_STR:
				//if(!(cin >> noskipws >> s1)) panic("Couldn't read a string.");
				getline(cin, s1);
				push(s1);
				break;
			case DECLARE:
				definestore(pc->s, curscope);
				break;
			case CALL:
				if (typetop() == ObjT) {
					operate("__call__", 2); // call should curry if it needs more args.
				} else {
					
					call(pc->i.get_si());
				}
				break;
			case DUP_TWO:
				if (stack.size() < 2) panic("Tried to dup second item of 1 or 0 item stack. (DUP_TWO).");
				push(*stack[stack.size()-2]);
				break;
			case LOAD_LIST:
				push(static_cast<List *>(nullptr));
				break;
			case CONS:
				l = popList();
				for (index = 0; index < pc->i; index++) {
					l = cons(putstore(pop()), l);
				}
				push(l);
				break;
			case HEAD:
				l = popList();
				if (l == nullptr)
					panic("Can't take head of empty list.");
				push(*store[l->head]);
				break;
			case TAIL:
				l = popList();
				if (l == nullptr)
					panic("Can't take tail of empty list.");
				push(l->tail);
				break;
			case LOAD_OBJ:
				push(new Object);
				break;
			case MOD_REC:
				o = popObj();
				for (index = 0; index < pc->i; index++) {
					s1 = popStr();
					(*o)[s1] = putstore(pop());
				}
				push(o);
				break;
			case GET_REC:
				o = popObj();
				s1 = popStr();
				push(get(o, s1));
				break;
			case HASSIGN:
				v1 = pop();
				l = popList();
				if (l == nullptr)
					panic("Can't assign head of empty list.");
				l->head = putstore(v1);
				break;
			case TASSIGN:
				l = popList();
				if (l == nullptr)
					panic("Can't assign head of empty list.");
				l->tail = popList();
				push(static_cast<List *>(l));
				break;
			case EOFOP:
				// TODO memory
				exit(0);
			case BEGIN_BLOCK:
				curscope = new Scope(Env(), curscope);
				break;
			case END_BLOCK:
				curscope = curscope->parent; // TODO memory management
				break;
			case END_DECLARES:
				// Now we put everything in the parent scope into local scope, preserving our new defs
				// TODO this should mean we only need to check local scope ever.
				
				if(curscope->parent != nullptr) // TODO 
					curscope->env.insert(curscope->parent->env.begin(),
										curscope->parent->env.end());
				
				
				break;
			case POP:
				pop();
				break;
			case COMPREHEND:
				s1 = popStr();
				l = static_cast<List *>(nullptr);
				for (auto it = s1.rbegin(); it!=s1.rend(); it++) {
					v1 = StackVal(string(1, *it));
					l = cons(putstore(v1), l);
				}
				push(l);
				break;
			default:
				{}
		}

	}



}



int main(int argc, char* argv[]) {
	if(argc != 2) panic("Usage: inter [file].");
	ifstream f(argv[1]);
	if(!f) panic("Couldn't open file.");
	
	setup(&f);
	
	runBytes(program.begin(), program.end(), nullptr);


	return 0;
}
