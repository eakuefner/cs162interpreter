#include "machinemanip.hh"
#include "panic.hh"

#include "debug.hh"

#include <sstream> // TODO I hate myself.

/*
 *	********** All sorts of helper functions. **********
 */
string typestrings[TYPES] = {"string", "boolean", "integer", "closure", "unit", "list", "object"};

IMPtype typetop() {
	return stack.back()->mytype;
}

void type_check(IMPtype expected, IMPtype got) { // check if the types are equal. if not, explode.
	if (expected == got) return;
	stringstream ss;
	ss << "Expected " << typestrings[expected] << "; got " << typestrings[got] << "." << endl;
	panic(ss.str());
}

mpz_class intVal(StackVal t) {
	type_check(IntT, t.mytype);
	return *t.val.i;
}
string strVal(StackVal t) {
	type_check(StrT, t.mytype);
	return *t.val.s;
}
bool boolVal(StackVal t) {
	type_check(BoolT, t.mytype);
	return t.val.b;
}
Closure * closureVal(StackVal t) {
	type_check(ClosureT, t.mytype);
	return t.val.c;
}
bool unitVal(StackVal t) { // only returns on success
	type_check(UnitT, t.mytype);
	return true;
}
List* listVal(StackVal t) {
	type_check(ListT, t.mytype);
	return t.val.l;
}
Object* objVal(StackVal t) {
	type_check(ObjT, t.mytype);
	return t.val.o;
}



StackVal pop() {
	if (stack.size() == 0) panic("Popping empty stack.");
	StackVal r = *stack.back();
	delete stack.back();
	stack.pop_back();
	return r;
}


mpz_class popInt() {
	return intVal(pop());
}
string popStr() {
	return strVal(pop());
}
bool popBool() {
	return boolVal(pop());
}
Closure * popClosure() {
	return closureVal(pop());
}
bool popUnit() { // only returns on success
	return unitVal(pop());
}
List* popList() {
	return listVal(pop());
}
Object * popObj() {
	return objVal(pop());
}


void push(mpz_class i) {
	stack.push_back(new StackVal(i));
}
void push(string s) {
	stack.push_back(new StackVal(s));
}
void push(bool b) {
	stack.push_back(new StackVal(b));
}
void push(Closure * c) {
	stack.push_back(new StackVal(c));
}
void push(List *l) {
	stack.push_back(new StackVal(l));
}
void push(Object * o) {
	stack.push_back(new StackVal(o));
}
// Unit
void push() {
	stack.push_back(new StackVal());
}

void push(StackVal sv) {
	StackVal * t = new StackVal();
	t->set(sv);
	stack.push_back(t);
}


/*
*** SCOPING ***

*** THIS IS NOW FALSE ***

In order for scoping to work properly, we need a sort of tree structure.
To accomplish this, a Scope is an environment together with a pointer to
a parent scope (or a nullptr for the root scope). Lookup is done by
checking then environment associated with a given scope, and then the
parent environment, and so on. This makes lookups of things not in local
scope somewhat expensive. Tough.
*/




StackVal* lookupLocal(string s, Scope* e) {
	if(e == nullptr) return nullptr;
		
	auto f = e->env.find(s);
	
	if(f == e->env.end()) //ie we didn't find anything
		return nullptr;
		
	unsigned int l = (*f).second;
	if(store.size()<=l) panic("Looking outside store.");
		
	return store[l];
}

StackVal* lookup(string s, Scope* e) {
	auto sv = lookupLocal(s, e);
		
	if(sv != nullptr) return sv; // TODO maybe just if(sv)
	
	if(e->parent == nullptr) {
		stringstream t;
		t << "Variable " << s << " not defined.";
		panic(t.str());
	}
	return lookup(s, e->parent);
}





bool definedLocal(string s, Scope* e) {
	return lookupLocal(s, e) != nullptr;
}

bool defined(string s, Scope* e) {
	return lookup(s, e) != nullptr;
}




// TODO actually remove things from the store

int allocstore() {
	store.push_back(new StackVal);
	return store.size() - 1;
}
int putstore(StackVal sv) {
	int r = allocstore();
	store[r]->set(sv);
	return r;
}

void definestore(string s, Scope* e) { // defines in local scope.
	if(definedLocal(s, e)) panic("Variable is already defined locally.");
	
	/*
	
	unsigned int at;

	if(free_store.empty()) {
		store.push_back(new StackVal());
		at = store.size()-1;
	}
	else {
		at = free_store.back();
		free_store.pop_back();
		//store[at]->set(v); // Just leave it: it'll get set later.
	}
	*/
	
	e->env[s] = allocstore();
}

void setstore(string s, Scope* e, StackVal v) {
	StackVal* sv = lookup(s, e); 
	if(sv == nullptr) panic("Variable set before being defined.");

	sv->set(v);
}

