#ifndef OPCODES_HH
#define OPCODES_HH
/*
 * Spacer to start opcodes on line 10.
 * I have line numbers.
 * You might not.
 */
//FOR NEW OPCODES: First append your opcode to the end of this list.
enum Opcode {
	NOP, // not used	// 0
	STORE,
	LABEL,
	POP_JUMP_IF_FALSE,	// 3
	JUMP,
	OUTPUT,
	LOAD_NUM,			// 6
	LOAD_BOOL,
	LOAD_STR,
	LOAD_UNIT,			// 9
	LOAD_VAR,
	NOT,
	PLUS,				// 12
	SUB,
	MUL,
	DIV,				// 15
	AND,
	OR,
	EQUAL,				// 18
	LEQ,
	INPUT_NUM,
	INPUT_STR,			// 21
	DECLARE,
	BEGIN_BLOCK,
	END_BLOCK,			// 24
	CONSTS,
	CONST_NUM,
	CONST_BOOL,			// 27
	CONST_STR,
	BEGIN_FUNC,
	END_FUNC,			// 30
	LOAD_FUNC,
	CALL,
	LOAD_LIST,			// 33
	HEAD,
	TAIL,
	CONS,				// 36
	LOAD_OBJ,
	MOD_REC,
	GET_REC,			// 39
	DUP_TWO,
	HASSIGN,
	TASSIGN,			// 42
	END_DECLARES,
	POP,
	COMPREHEND,			// 45
	EOFOP
};
#endif

