#ifndef MACHINEMANIP_HH
#define MACHINEMANIP_HH

#include <string>
#include <sstream>
#include <iostream>

#include "globals.hh"

using namespace std;


IMPtype typetop();

void type_check(IMPtype, IMPtype);

StackVal pop();


mpz_class	intVal(StackVal t);
string	strVal(StackVal t);
bool	boolVal(StackVal t);
Closure*	closureVal(StackVal t);
bool	unitVal(StackVal t);
List*	listVal(StackVal t);
Object* objVal(StackVal t);

mpz_class	popInt();
string	popStr();
bool	popBool();
Closure*	popClosure();
bool	popUnit();
List*	popList();
Object* popObj();

void	push(mpz_class i);
void	push(string s);
void	push(bool b);
void	push(Closure * c);
void	push(StackVal v);
void	push(List *l);
void	push(Object *o);
void	push(); // Unit

int putstore(StackVal);

void definestore(string s, Scope* e);
void setstore(string s, Scope* e, StackVal v);

StackVal* lookup(string s, Scope* e);

#endif

