#!/bin/bash
filename=tmp.`date +%s`.bytecode
scala -classpath ../compiler notJScheme $1 --compile > $filename
if [ $? -ne 0 ]; then
    echo "Compile failed."
    cat $filename
else
	../interpreter/inter $filename
fi
rm $filename
