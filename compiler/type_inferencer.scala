package cs162.notJScheme.typeInferencer


import java.io._
import scala.io._
import cs162.notJScheme.syntax._


//---------- TYPE INFERENCER ENTRY POINT ----------


object Typer {




//---------- TYPE INFERENCER SEMANTIC DOMAINS ----------

// abstract machine configuration
case class Config(t:Term, env:Env)

// environment: Var -> Type
case class Env(env:Map[Var, Type] = Map()) {
  def apply(x:Var): Type = env get x match {
    case Some(typ) => typ
    case None => throw illTyped("free variable")
  }
  def +(tup:Tuple2[Var, Type]): Env = Env(env + tup)
}

// environment companion object
object Env { 
  implicit def env2map(e:Env): Map[Var, Type] = e.env
  implicit def map2env(m:Map[Var, Type]): Env = Env(m)
}

// exception to be thrown when a program is ill-typed
case class illTyped(msg:String) extends Exception(msg)

//---------- SEMANTIC HELPER FUNCTIONS ----------

object SemanticHelpers {
  // lift program to initial configuration
  def inject(prog:Program): Config = { Config(prog.t, Env()) }

  // union two types. unlike the semantics this actually performs the
  // type unification using the union-find data structure; hence it
  // returns Unit instead of Bool and throws an illTyped exception if
  // the unification fails
  //
  // NOTE: the find() function should use path compression, but this
  // union() function should NOT use union-by-rank; when unioning a
  // type variable T with something else, T always sets its parent to
  // that other type
  def union(type1:Type, type2:Type): Unit = {
      
      //The below function will determine whether it is possible to union
      def unification(t1:Type, t2:Type): Boolean = {
          val numstr = Set[Type](NumT(), StrT())
          (t1, t2) match {
              case (_,_) if (t1 == t2) => true
              case (x:TVar, y) if (x.a == AL && !((numstr + TVar(x.x,None) + x)(y))) => false
              case (x, y:TVar) if (y.a == AL && !((numstr + TVar(y.x,None) + y)(x))) => false
              case (x:TVar, y) if (!varsIn(y)(x)) => true
              case (x, y:TVar) if (!varsIn(x)(y)) => true  
              case (ListT(t1p), ListT(t2p)) => {
            union(t1p, t2p)
		true
	      }
              case (FunT(t3s, t4), FunT(t5s, t6)) => {
	      	union(t4, t6)
		(t3s zip t5s).map(x => union(x._1, x._2))
		true
	      }
              case _ => false;
           }
      }
      
      val t1 = find(type1)
      val t2 = find(type2)

      if (t1 != t2) {
      if (unification(t1, t2)) {
	    (t1, t2) match {
	    	case (x:TVar, y) => x.parent = Some(y)
		case (x, y:TVar) => y.parent = Some(x)
		case _ => Unit
	}
      } else {
            throw illTyped("tried to unify incompatible types")
     }
     }
  }

  // return a type's set representative; this function should use path
  // compression to optimize performance
  def find(typ:Type): Type = {
  	typ match {
		case x:TVar => {
			x.parent match {
				case None => x
				case Some(y) => {
					val tmp = find(y)
					x.parent = Some(tmp)
					tmp
				}
			}
		}
		case x => x
      }
  }

  // return all the type variables in a type
  def varsIn(typ:Type): Set[TVar] = {
  	typ match {
      case _:NumT | _:BoolT | _:StrT | _:UnitT => Set();
      case x : TVar => Set(x);
      case ListT(x) => varsIn(x);
      case FunT(ts, t) => (ts.map(varsIn).foldLeft[Set[TVar]](Set())(_|_)) | varsIn(t)
  }
  }

}

//---------- TYPE INFERENCER ----------

object Infer {
  import SemanticHelpers._

  def eval(config:Config): Type = {
    val env = config.env

    // since we'll be making lots of recursive calls where the
    // environment doesn't change, we'll define an inner function that
    // will leave env as a free variable
    def evalTo(t:Term): Type = t match {
      case Seq(t1, t2) => {
          evalTo(t1)
          evalTo(t2)
      }
      case Assign(x, e) => {
          union(env(x), evalTo(e))
          UnitT()
      }
      case w @ While(e, t) => {
          union(evalTo(e), BoolT())
          evalTo(t)
          UnitT()
      }
      case Out(e) => {
          evalTo(e)
          UnitT()
      }
      case HAssign(e1, e2) => {
          union(evalTo(e1), ListT(TVar()))
          union(evalTo(e2), TVar())
          UnitT()
      }
      case TAssign(e1, e2) => {
          union(evalTo(e1), ListT(TVar()))
          union(evalTo(e2), ListT(TVar()))
          UnitT()
      }
      case Num(n) => NumT()
      case Bool(b) => BoolT()
      case Str(str) => StrT()
      case NotUnit() => UnitT()
      case x:Var => {env(x)}
      case Not(e) => {
          val t = evalTo(e)
          union(t, BoolT())
          t
      }
      case BinOp(bop, e1, e2) => bop match {
          case And | Or => {
              union(evalTo(e1), BoolT())
              union(evalTo(e2), BoolT())
              BoolT()
          }
          case Mul | Div | Sub => {
              union(evalTo(e1), NumT())
              union(evalTo(e2), NumT())
              NumT()
          }
          case Add => {
              val t1 = evalTo(e1)
              union(t1, TVar(AL))
              union(evalTo(e2), TVar(AL))
              t1
          }
          case Lte => {
              union(evalTo(e2), TVar(AL))
              union(evalTo(e2), TVar(AL))
              BoolT()
          }
      	  case Cons => {
              val t1 = evalTo(e1)
              val t2 = evalTo(e2)
              union(t2, ListT(t1))
              t2
          }
	  case Equal => {
	      BoolT()
	  }
      }
      case If(e, t1, t2) => {
          union(evalTo(e), BoolT())
          val t = evalTo(t1)
          union(t, evalTo(t2))
          t
      }
      case In(typ) => {
          union(typ, TVar(AL))
          typ
      }
      case Call(ef, es) => {
      	  val tau = evalTo(ef)
	  val taups = es.map(evalTo)
	  val tvs = es.map(x => TVar())
	  val tp = TVar()
	  union(tau, FunT(tvs, tp))
	  tvs.zip(taups).map(a => union(a._1, a._2))
	  tp
      }
      case NotList(es) => {
          if (es == List()) {ListT(TVar())}
          else {
              val t = evalTo(es.head)
              es.map(evalTo).map(union(t,_))
              ListT(t)
          }
      }
      case Head(e)  => {
          val t = TVar()
          union(evalTo(e), ListT(t))
          t
      }
      case Tail(e)  => {
          val t = TVar()
	  val tau = evalTo(e)
          union(tau, ListT(t))
          tau
      }
      case Block(vbs, t) => {
          val tvs = vbs.map(_ => TVar())
          val envp = vbs.map(a => a.x).zip(tvs).foldLeft(env)(_+_)
          val ts = vbs.map(x => (eval(Config(x.e, envp))))
          ts.zip(tvs).map(a => union(a._1,a._2))
          eval(Config(t,envp))
      }
      case Fun(f, xs, t) => {
          val tvs = xs.map(_ => TVar())
          val tp = TVar()
          val envp = xs.zip(tvs).foldLeft(env)(_+_) + (f, tp)
          val tau = eval(Config(t, envp))
          union(tp, FunT(tvs, tau))
          tp
      }
      case _ => throw illTyped("not valid notJScheme (do you have objects?)")
    }

    evalTo(config.t)
  }

}

	val run = (ast: Program) => {
		import SemanticHelpers._
		import Infer._
		println(find(eval(inject(ast))))
	}

}

