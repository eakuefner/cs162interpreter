#include "globals.hh"



std::vector<StackVal*> stack;

std::vector<Instruction> program;

std::map<int, int> labels; // label number => program counter

std::vector<Scope*> scopes;

std::vector<StackVal*> store;

std::vector<StackVal*> consts;

std::vector<unsigned int> free_store;

std::vector<std::pair<unsigned int, unsigned int> > functions;

StackVal::~StackVal() {
	if (mytype == IntT) {
		//delete val.i;
		//val.i = nullptr;
	}
	if (mytype == StrT) {
		//delete val.s;
		//val.s = nullptr;
	}
	if (mytype == ClosureT) {
		//delete val.c;
		//val.c = nullptr;
	}
	if (mytype == ObjT) {
		//delete val.o;
		//val.o = nullptr;
	}
}

