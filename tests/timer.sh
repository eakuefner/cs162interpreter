#!/bin/bash

ulimit -s 16384

filename=tmp.`date +%s`.bytecode
echo Timing program on stock interpreter:
time scala -J-Xss64m -J-Xmx1024m -classpath ../compiler notJScheme $1
echo Timing compile:
time scala -J-Xss64m -classpath ../compiler notJScheme $1 --compile > $filename
echo Timing program on bytecode interpreter:
time ../interpreter/inter $filename
rm $filename
