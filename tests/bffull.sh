#! /bin/bash

# translate the brainfuck

k=$(echo "256
$(cat tests/hw.bf)" | python tests/bfe.py | grep "Encoded code" | cut -d' ' -f 3)


# make the notJScheme -> bytecodes compiler

cd compiler
fsc *.scala
scala notJScheme ../tests/bf.not --compile > ../tests/bf.bytecodes
cd ..


# make the bytcode interpreter

cd interpreter
t=$(make clean)
t=$(make)


# actually run the bytecode interpreter

o=$(echo "256
$k
0" | ./inter ../tests/bf.bytecodes)


# parse the output

cd ..
r=$(echo $o | cut -d'O' -f 2 | cut -d' ' -f 2)
echo "256
$r" | python tests/bfd.py | sed -n 4p
