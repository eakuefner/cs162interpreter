#include "panic.hh"

void panic(std::string error) {
        std::cerr << "PANIC :: " << error << std::endl;
        exit(1);
}

