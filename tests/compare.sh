#!/bin/bash

ulimit -s 16384

filename=tmp.`date +%s`.$$.bytecode

scala -J-Xss64m -J-Xmx1024m -classpath ../compiler notJScheme $1
echo ================
scala -J-Xss64m -classpath ../compiler notJScheme $1 --compile > $filename
../interpreter/inter $filename

rm $filename

