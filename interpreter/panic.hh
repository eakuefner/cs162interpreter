#ifndef PANIC_HH
#define PANIC_HH

#include <string>
#include <iostream>

void panic(std::string error);

#endif

