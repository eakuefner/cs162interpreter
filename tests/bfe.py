#! /usr/bin/env python

from sys import stdin


syms = 9

cdict = { ">":1, "<":2, "+":3, "-":4, "[":5, "]":6, ",":7, ".":8 }

print("How large of cells do you want?")
cs = int(stdin.readline())
print("Brainfuck code:")
code = stdin.readline()[::-1]
code = [ cdict[i] for i in code if cdict.has_key(i) ]

if cs is not 256:
	print("Input, as a comma-separated list of integers:")
	inp = stdin.readline().split(",")[::-1]
	inp = [int(i) for i in inp]
else:
	print("Input, as ASCII:")
	inp = stdin.readline()[:-1]
	inp = [ord(i) for i in inp]

iCode = 0
for i in code:
	iCode*=syms
	iCode+=i

iIn = 0
for i in inp:
	iIn*=cs
	iIn+=i

print "Cell size:", cs
print "Encoded code:", iCode
print "Encoded input:", iIn
